import React, { Component } from "react";
import axios from "axios";
import app from "../keyAndToken.js";

class UpdateListName extends Component {

  constructor(props) {

    super(props);

    this.state = {
      modalStatus: ""
    };

  }
  
  submitListName = event => {
    event.preventDefault();
    const formData = new FormData(event.target);
    const submittedListName = formData.get("list-name");
    
    // update the List's name
    axios.put(`https://api.trello.com/1/lists/${this.props.listId}?name=${submittedListName}&key=${app.key}&token=${app.token}`)
    .then(response => {
      this.props.updateListName(submittedListName);
      this.closeModal();
    })
    .catch(err => {
      console.log(err);
    });

  };

  showModal = () => {
    this.setState({
      modalStatus: "is-active"
    });
  };

  closeModal = () => {
    this.setState({
      modalStatus: ""
    });
  };

  render() {
    
    const { modalStatus } = this.state;

    return (
      <div className="card-header-title">

        <span className="Update-list-name"
              onClick={this.showModal}
        >
          {this.props.listName}
        </span>
          
        <div className={`modal ${modalStatus}`}>
          <div className="modal-background"></div>
          <div className="modal-content">
            <form onSubmit={this.submitListName}>
              <div className="field">
                <div className="control">
                  <input className="input" 
                         type="text" 
                         placeholder="New List name" 
                         name="list-name"
                         required
                  />
                </div>
              </div>
              <div className="field">
                <div className="control">
                  <button className="button is-link">
                    Update
                  </button>
                </div>  
              </div>
            </form>
          </div>
          <button className="modal-close is-large" 
                  aria-label="close"
                  onClick={this.closeModal}
          ></button>
        </div>

      </div>
    );
  }
}

export default UpdateListName;
