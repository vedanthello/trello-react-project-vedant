import React, { Component } from "react";
import axios from "axios";
import app from "../keyAndToken.js";

class ArchiveList extends Component {

  archiveList = () => {
    // PUT request to archive List
    axios.put(`https://api.trello.com/1/lists/${this.props.listId}?closed=true&key=${app.key}&token=${app.token}`)
    .then(response => {
      this.props.removeArchivedList(this.props.listId);
    })
    .catch(err => {
      console.log(err);
    });

  };

  render() {
    return (
      <div className="card-header-icon ArchiveList">
        <div className="dropdown is-hoverable">
          <div className="dropdown-trigger">
            <button className="button is-white has-text-grey-light" onClick={this.archiveList}>
              <span className="icon">
                <i className="fa fa-archive" aria-hidden="true"></i>
              </span>
            </button>
          </div>
          <div className="dropdown-menu">
            <div className="dropdown-content">
              <div className="dropdown-item">
                Archive This List
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ArchiveList;
