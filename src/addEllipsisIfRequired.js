/**
 * Function parameters:
 * string - a string
 * 
 * This function shortens the string if it long and then adds
 * ellipsis to the shortened string and returns the result
 */
export const addEllipsisIfRequired = string => {
  
  let result = "";
  if (string.length >= 28) {
    result = string.slice(0, 27);
    result = result.trimEnd();
    result += "...";
  } else {
    result = string;
  }

  return result;
  
};