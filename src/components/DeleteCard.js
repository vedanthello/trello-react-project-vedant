import React, { Component } from "react";
import axios from "axios";
import app from "../keyAndToken.js";

class DeleteCard extends Component {

  constructor(props) {
    super(props);
  
    this.state = {
      popoverStatus: "",
      deleteStatus: ""
    };
  }

  deleteCard = () => {

    // turn the "Delete" button into its loading version
    this.setState({ deleteStatus: "is-loading" });

    // DELETE request to delete Card
    axios.delete(`https://api.trello.com/1/cards/${this.props.cardId}?key=${app.key}&token=${app.token}`)
    .then(response => {
      this.props.removeDeletedCard(this.props.cardId);
    })
    .catch(err => {
      console.log(err);
    });

  };
  
  showPopover = () => {
    this.setState({ popoverStatus: "is-active" });
  };

  hidePopover = () => {
    this.setState({ popoverStatus: "" });
  };

  render() {
    return (
      <div className="DeleteCard">

        <div className={`dropdown is-up ${this.state.popoverStatus}`}>
          <div className="dropdown-trigger">
            <button className="button is-danger is-light" 
                    onClick={this.showPopover}
            >
              Delete Card
            </button> 
          </div>
          
          <div className="dropdown-menu">
            <div className="dropdown-content">
              <div className="dropdown-item">
                <p className="Card-delete-warning-text"><b>Are you sure you want to delete this Card? There is no undo.</b></p>
              </div>
              <div className="dropdown-item">
                <div className="field is-grouped">
                  <div className="control">
                    <button className={`button is-danger ${this.state.deleteStatus}`} 
                            onClick={this.deleteCard}
                    >
                      Delete
                    </button>
                  </div>
                  <div className="control">
                    <button className="button" onClick={this.hidePopover}>
                      Cancel
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>
    );
  }
}

export default DeleteCard;
