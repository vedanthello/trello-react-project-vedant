import React, { Component } from "react";
import axios from "axios";
import app from "../keyAndToken.js";

class CardDescription extends Component {

  constructor(props) {
    super(props);
  
    this.state = {
      description: "",
      isEditing: false 
    };
  }
  
  componentDidMount() {
    // get the Card's Description
    axios.get(`https://api.trello.com/1/cards/${this.props.cardId}?key=${app.key}&token=${app.token}`)
    .then(response => {
      this.setState({ description: response.data.desc });
    })
    .catch(err => {
      console.log(err);
    });
  }

  submitDescription = event => {
    event.preventDefault();
    const formData = new FormData(event.target);
    let submittedDescription = formData.get("description");
    submittedDescription = submittedDescription.trim();
    submittedDescription = submittedDescription.replace(/\n/g, "%0A")
    
    // update the Card's Description
    axios.put(`https://api.trello.com/1/cards/${this.props.cardId}?desc=${submittedDescription}&key=${app.key}&token=${app.token}`)
    .then(response => {
      this.setState({ description: response.data.desc });
      this.closeEditor();
    })
    .catch(err => {
      console.log(err);
    });
  }

  showEditor = () => {
    this.setState({ isEditing: true });
  };

  closeEditor = () => {
    this.setState({ isEditing: false });
  };

  render() {
    return (
      <div className="content CardDescription">

        <h2>Description</h2>
        {
          (this.state.isEditing === false) ? (
            (this.state.description === "") ? (
              <textarea className="textarea has-fixed-size Before-editing"
                        placeholder="Add a more detailed description..."
                        rows="2"
                        onClick={this.showEditor}
              >
              </textarea>
            ) : (
              <p className="Edits-saved" 
                 onClick={this.showEditor}
              >
                {this.state.description}
              </p>
            )
          ) : (
            <form onSubmit={this.submitDescription}>
              <div className="field">
                <div className="control">
                  <textarea className="textarea is-focused"
                            placeholder="Add a more detailed description..."
                            defaultValue={this.state.description}
                            name="description"
                            autoFocus
                  ></textarea>
                </div>
              </div>
              <div className="field is-grouped">
                <div className="control">
                  <button className="button is-link">
                    Update
                  </button>
                </div>
                <div className="control">
                  <div className="button" onClick={this.closeEditor}>
                    Cancel
                  </div>
                </div>
              </div>
            </form>
          )
        }

      </div>
    );
  }
}

export default CardDescription;
