import React, { Component } from 'react';

class Home extends Component {
  render() {
    return (
      <div className="hero is-medium is-bold">
        <div className="hero-body">
          <div className="container">
            <h1 className="title">
              Welcome to Trello
            </h1>
            <h2 className="subtitle">
              Click on Boards on the bar above to view your boards
            </h2>
          </div>
        </div>
      </div>    
    );
  }
}

export default Home;
