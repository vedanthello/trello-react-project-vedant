import React, { Component } from "react";
import axios from "axios";
import app from "../keyAndToken.js";
import { Link } from "react-router-dom";
import { addEllipsisIfRequired } from "../addEllipsisIfRequired.js";
import CreateNewBoard from "./CreateNewBoard.js";

class Boards extends Component {

  constructor(props) {
    super(props);

    this.state = {
      boards: [],
      errorGettingBoards: ""
    };
  }

  componentDidMount() {
    axios.get(`https://api.trello.com/1/members/me/boards?key=${app.key}&token=${app.token}`)
    .then(response => {
      this.setState({ boards: response.data });
    })
    .catch(err => {
      this.setState({ errorGettingBoards: "Error in retrieving Boards" });
      console.log(err);
    });
  }

  render() {
    const { boards, errorGettingBoards } = this.state;

    return (
      <div>
        
        <h1 className="Boards_heading has-text-weight-semibold is-size-5">Boards</h1>

        {
          errorGettingBoards && (
            <div className="px-4">
              <button className="button is-danger is-light is-outlined is-static is-fullwidth is-medium">{errorGettingBoards}</button>
            </div>
          )
        }

        <div className="columns is-multiline is-marginless">
          {
            boards.map(board => {
              return (
                <Link to={`/boards/${board.id}`} key={board.id} className="column is-one-quarter is-flex">
                  <p className="notification has-background-info has-text-white has-text-weight-semibold">{addEllipsisIfRequired(board.name)}</p>
                </Link>
              );
            })
          }
        </div>
  
        <CreateNewBoard history={this.props.history} />

      </div>
    );
  }
}

export default Boards;
