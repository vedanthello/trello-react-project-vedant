import React, { Component } from "react";
import axios from "axios";
import app from "../keyAndToken.js";

class CreateNewBoard extends Component {

  constructor(props) {
    super(props);

    this.state = {
      modalStatus: "",
      newBoardName: "",
      newBoardId: "",
      errorCreatingBoard: ""
    };
  }
  
  handleNewBoardNameChange = event => {
    this.setState({ newBoardName: event.target.value });
  }

  createNewBoardThenNavigate = () => {
    if (this.state.newBoardName !== "") {
      axios.post(`https://api.trello.com/1/boards?name=${this.state.newBoardName}&defaultLists=false&key=${app.key}&token=${app.token}`)
      .then(response => {
        this.props.history.push(`/boards/${response.data.id}`);
      })
      .catch(err => {
        this.setState({ errorCreatingBoard: "Error creating Board" });
        console.log(err);
      });
    }
  };

  showModal = () => {
    this.setState({
      modalStatus: "is-active"
    });
  };

  closeModal = () => {
    this.setState({
      modalStatus: "", 
      errorCreatingBoard: ""
    });
  };

  render() {

    const { errorCreatingBoard } = this.state;
    
    return (
      <div>
        
        <div className="tile is-parent">
          <a className="tile is-child box has-background-info has-text-white has-text-weight-semibold"
             onClick={this.showModal}
          >
            <span className="icon">
              <i className="fa fa-plus"></i>
            </span>
            <span>Create new Board</span>
          </a>
        </div>

        <div className={`modal ${this.state.modalStatus}`}>
          <div className="modal-background"></div>
          <div className="modal-content">
            <div className="field">
              <div className="control">
                <input className="input" 
                       type="text" 
                       placeholder="Enter Board name" 
                       value={this.state.newBoardName}
                       onChange={this.handleNewBoardNameChange}
                       required
                />
              </div>
            </div>
            <div className="field">
              <div className="control">
                <button className="button is-link" onClick={this.createNewBoardThenNavigate}>
                  Create Board
                </button>
              </div>  
            </div>
            {
              errorCreatingBoard && (
                <button className="button is-danger is-light is-outlined is-static is-fullwidth is-medium">{errorCreatingBoard}</button>
              )
            }
          </div>
          <button className="modal-close is-large" 
                  aria-label="close"
                  onClick={this.closeModal}
          ></button>
        </div>

      </div>
    );
  }
}

export default CreateNewBoard;
