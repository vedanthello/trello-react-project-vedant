import React, { Component } from 'react';
import axios from "axios";
import app from "../keyAndToken.js";
import UpdateBoardName from "./UpdateBoardName.js";
import SingleList from "./SingleList.js";
import AddAList from "./AddAList.js";

class SingleBoard extends Component {

  constructor(props) {

    super(props);
    this.state = {
      listIds: [],
      errorGettingLists: ""
    };

  }
 
  removeArchivedList = givenListId => {
    this.setState(state => ({
      listIds: state.listIds.filter(listId => givenListId !== listId)
    }));
  }

  renderAddedList = givenListId => {
    this.setState(state => ({
      listIds: [...state.listIds, givenListId]
    }));
  };

  componentDidMount() {

    const { boardId } = this.props.match.params;

    // get the Ids of all the Lists on the Board
    axios.get(`https://api.trello.com/1/boards/${boardId}/lists?key=${app.key}&token=${app.token}`)
    .then(response => {
      const lists = response.data;
      this.setState({listIds: lists.map(list => list.id)});
    })
    .catch(err => {
      this.setState({ errorGettingLists: "Error in retrieving Lists" });
      console.log(err);
    });

  }

  render() {
    const { listIds, errorGettingLists } = this.state;

    return (
      <div className="SingleBoard">

        <div>
          <UpdateBoardName boardId={this.props.match.params.boardId} />
        </div>        

        {
          errorGettingLists && (
            <div className="px-4 mb-4">
              <button className="button is-danger is-light is-outlined is-static is-fullwidth is-medium">{errorGettingLists}</button>
            </div>
          )
        }

        <div className="List-stack-and-add-list">
          {
            listIds.map(listId => <SingleList key={listId} listId={listId} removeArchivedList={this.removeArchivedList} />)
          }
          
          <AddAList boardId={this.props.match.params.boardId} 
                    renderAddedList={this.renderAddedList} 
          />  
        </div>

      </div>
    );
  }
}

export default SingleBoard;
