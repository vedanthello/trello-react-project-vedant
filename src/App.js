import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import trelloLogo from "./trello-logo.png";
import Boards from "./components/Boards.js";
import SingleBoard from "./components/SingleBoard.js";
import Home from "./components/Home.js";
import NotFound from "./components/NotFound.js";

function App() {
  return (
    <Router>
      <div className="App">
        <nav className="navbar has-background-info" role="navigation" aria-label="main navigation">
          <div className="navbar-brand">
            <div className="navbar-item">
              <img src={trelloLogo} alt="Trello Logo" />
            </div>
          </div>

          <Link to="/" className="navbar-item has-text-white has-background-info has-text-weight-semibold">
            Home        
          </Link>
          
          <Link to="/boards" className="navbar-item has-text-white has-background-info has-text-weight-semibold">
            Boards
          </Link>
        </nav>   

        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/boards" render={routeProps => <Boards history={routeProps.history} />} />
          <Route path="/boards/:boardId" component={SingleBoard} />
          <Route path="*" component={NotFound} />
        </Switch>
      </div>
    </Router>
  );
}

export default App;
