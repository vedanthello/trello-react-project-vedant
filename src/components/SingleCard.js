import React, { Component } from "react";
import axios from "axios";
import app from "../keyAndToken.js";
import UpdateCardName from "./UpdateCardName.js";
import DeleteCard from "./DeleteCard.js";
import CardDescription from "./CardDescription.js";

class SingleCard extends Component {

  constructor(props) {
    super(props);

    this.state = {
      cardName: "",
      modalStatus: ""
    };
  }

  updateCardName = cardName =>  {
    this.setState({ cardName });
  };

  componentDidMount() {
    const { cardId } = this.props;

    // get the Card's name
    axios.get(`https://api.trello.com/1/cards/${cardId}?key=${app.key}&token=${app.token}`)
    .then(response => {
      this.setState({ cardName: response.data.name });
    })
    .catch(err => {
      console.log(err);
    });
  }

  showModal = () => {
    this.setState({
      modalStatus: "is-active"
    });
  };

  closeModal = () => {
    this.setState({
      modalStatus: ""
    });
  };

  render() {
    return (
      <div className="SingleCard">

        <button className="button is-fullwidth is-light Card-button"
                onClick={this.showModal}
        >
          {this.state.cardName}
        </button>

        <div className={`modal ${this.state.modalStatus}`}>
          <div className="modal-background"></div>
          <div className="modal-card Card-modal-box">
            <header className="modal-card-head Card-modal-header">
              <UpdateCardName cardName={this.state.cardName}
                              cardId={this.props.cardId}
                              updateCardName={this.updateCardName} 
              />
              <button className="delete" aria-label="close" onClick={this.closeModal}></button>
            </header>
            <section className="modal-card-body">
              <CardDescription cardId={this.props.cardId} />
            </section>
            <footer className="modal-card-foot">
              <DeleteCard removeDeletedCard={this.props.removeDeletedCard}
                          cardId={this.props.cardId}
              />
            </footer>
          </div>
        </div>

      </div>
    );
  }
}

export default SingleCard;
