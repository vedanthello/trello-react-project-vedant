import React, { Component } from "react";
import axios from "axios";
import app from "../keyAndToken.js";
import UpdateListName from "./UpdateListName.js";
import ArchiveList from "./ArchiveList.js";
import SingleCard from "./SingleCard.js";
import AddACard from "./AddACard.js";

class SingleList extends Component {

  constructor(props) {

    super(props);

    this.state = {
      listName: "",
      cardIds: [],
      errorGettingCards: ""
    };
    
  }

  updateListName = listName =>  {
    this.setState({ listName });
  };

  renderAddedCard = givenCardId => {
    this.setState(state => ({
      cardIds: [...state.cardIds, givenCardId]
    }));
  };

  removeDeletedCard = givenCardId => {
    this.setState(state => ({
      cardIds: state.cardIds.filter(cardId => givenCardId !== cardId)
    }));
  };

  componentDidMount() {

    const { listId } = this.props;

    // get the List's name
    axios.get(`https://api.trello.com/1/lists/${listId}?key=${app.key}&token=${app.token}`)
    .then(response => {
      this.setState({listName: response.data.name});
    })
    .catch(err => {
      console.log(err);
    });

    // get the Ids for all the Cards
    axios.get(`https://api.trello.com/1/lists/${listId}/cards?key=${app.key}&token=${app.token}`)
    .then(response => {
      const cards = response.data;
      this.setState({cardIds: cards.map(card => card.id)});
    })
    .catch(err => {
      this.setState({ errorGettingCards: "Error in retrieving Cards" });
      console.log(err);
    });
    
  }

  render() {

    const { errorGettingCards } = this.state;

    return (
      <div className="card SingleList">

        <header className="card-header List-header">
          <UpdateListName listId={this.props.listId}
                          listName={this.state.listName} 
                          updateListName={this.updateListName} 
          />
          <ArchiveList removeArchivedList={this.props.removeArchivedList}
                       listId={this.props.listId}
          />
        </header>

        <div className="card-content Card-stack">

          {
            errorGettingCards && (
                <button className="button is-danger is-light is-outlined is-static is-fullwidth">{errorGettingCards}</button>
            )
          }

          {
            this.state.cardIds.map(cardId => (
              <SingleCard key={cardId} 
                          cardId={cardId}  
                          removeDeletedCard={this.removeDeletedCard} 
              />
            )) 
          } 

        </div>

        <footer className="card-footer">
          <AddACard listId={this.props.listId} renderAddedCard={this.renderAddedCard}/>
        </footer>

      </div>
    );
  }
}

export default SingleList;
