import React, { Component } from "react";
import axios from "axios";
import app from "../keyAndToken.js";

class AddACard extends Component {

  constructor(props) {
    super(props);

    this.state = {
      modalStatus: "",
      errorAddingCard: ""
    };
  }
  
  submitCardName = event => {
    event.preventDefault();
    const formData = new FormData(event.target);
    const submittedCardName = formData.get("card-name");
    
    // POST request to create a new Card
    axios.post(`https://api.trello.com/1/cards/?name=${submittedCardName}&pos=bottom&idList=${this.props.listId}&key=${app.key}&token=${app.token}`)
    .then(response => {
      const idOfAddedCard = response.data.id;
      this.props.renderAddedCard(idOfAddedCard);
      this.closeModal();
    })
    .catch(err => {
      this.setState({ errorAddingCard: "Error adding Card" });
      console.log(err);
    });
  };

  showModal = () => {
    this.setState({
      modalStatus: "is-active"
    });
  };

  closeModal = () => {
    this.setState({
      modalStatus: "",
      errorAddingCard: ""
    });
  };

  render() {
    
    const { modalStatus, errorAddingCard } = this.state;

    return (
      <div className="card-footer-item AddACard">
        
        <button className="button is-light is-fullwidth" onClick={this.showModal}>
          <span className="icon">
            <i className="fa fa-plus"></i>
          </span>
          <span>Add a Card</span>
        </button>

        <div className={`modal ${modalStatus}`}>
          <div className="modal-background"></div>
          <div className="modal-content">

            <form onSubmit={this.submitCardName}>
              <div className="field">
                <div className="control">
                  <input className="input" 
                         type="text" 
                         placeholder="Enter Card name" 
                         name="card-name"
                         required
                  />
                </div>
              </div>
              <div className="field">
                <div className="control">
                  <button className="button is-link">
                    Add Card
                  </button>
                </div>  
              </div>
            </form>

            {
              errorAddingCard && (
                <button className="button is-danger is-light is-outlined is-static is-fullwidth is-medium mt-3">{errorAddingCard}</button>
              )
            }
          </div>
          <button className="modal-close is-large" 
                  aria-label="close"
                  onClick={this.closeModal}
          ></button>
        </div>

      </div>
    );
  }
}

export default AddACard;
