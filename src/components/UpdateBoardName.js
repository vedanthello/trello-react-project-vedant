import React, { Component } from "react";
import axios from "axios";
import app from "../keyAndToken.js";

class UpdateBoardName extends Component {

  constructor(props) {
    super(props);

    this.state = {
      modalStatus: "",
      boardName: ""
    };
  }
  
  submitBoardName = event => {
    event.preventDefault();
    const formData = new FormData(event.target);
    const submittedBoardName = formData.get("board-name");
    
    // update the board's name using the given board's Id
    axios.put(`https://api.trello.com/1/boards/${this.props.boardId}?name=${submittedBoardName}&key=${app.key}&token=${app.token}`)
    .then(response => {
      this.setState({boardName: response.data.name});
      this.closeModal();
    })
    .catch(err => {
      console.log(err);
    });

  };

  showModal = () => {
    this.setState({
      modalStatus: "is-active"
    });
  };

  closeModal = () => {
    this.setState({
      modalStatus: ""
    });
  };

  componentDidMount() {

    //get the board's name from the given board's Id    
    axios.get(`https://api.trello.com/1/boards/${this.props.boardId}?key=${app.key}&token=${app.token}`)
    .then(response => {
      this.setState({boardName: response.data.name});
    })
    .catch(err => {
      console.log(err);
    });
    
  }


  render() {
    
    const { modalStatus, boardName } = this.state;

    return (
      <div>

        <button className="button is-light has-text-weight-semibold is-medium"
                onClick={this.showModal}
        >
          {boardName}
        </button>

        <div className={`modal ${modalStatus}`}>
          <div className="modal-background"></div>
          <div className="modal-content">
            <form onSubmit={this.submitBoardName}>
              <div className="field">
                <div className="control">
                  <input className="input" 
                         type="text" 
                         placeholder="New Board name" 
                         name="board-name"
                         required
                  />
                </div>
              </div>
              <div className="field">
                <div className="control">
                  <button className="button is-link">
                    Update
                  </button>
                </div>  
              </div>
            </form>
          </div>
          <button className="modal-close is-large" 
                  aria-label="close"
                  onClick={this.closeModal}
          ></button>
        </div>

      </div>
    );
  }
}

export default UpdateBoardName;
