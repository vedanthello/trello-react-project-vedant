import React, { Component } from "react";
import axios from "axios";
import app from "../keyAndToken.js";

class AddAList extends Component {

  constructor(props) {
    super(props);

    this.state = {
      modalStatus: "",
      errorAddingList: ""
    };
  }
  
  submitListName = event => {
    event.preventDefault();
    const formData = new FormData(event.target);
    const submittedListName = formData.get("list-name");
    
    // POST request to create a new List
    axios.post(`https://api.trello.com/1/lists/?name=${submittedListName}&idBoard=${this.props.boardId}&pos=bottom&key=${app.key}&token=${app.token}`)
    .then(response => {
      const idOfAddedList = response.data.id;
      this.props.renderAddedList(idOfAddedList);
      this.closeModal();
    })
    .catch(err => {
      this.setState({ errorAddingList: "Error adding List" });
      console.log(err);
    });
  };

  showModal = () => {
    this.setState({
      modalStatus: "is-active"
    });
  };

  closeModal = () => {
    this.setState({
      modalStatus: "", 
      errorAddingList: ""
    });
  };

  render() {
    
    const { modalStatus, errorAddingList } = this.state;

    return (
      <div className="AddAList">
        
        <button className="button is-white is-fullwidth" onClick={this.showModal}>
          <span className="icon">
            <i className="fa fa-plus"></i>
          </span>
          <span>Add a List</span>
        </button>

        <div className={`modal ${modalStatus}`}>
          <div className="modal-background"></div>
          <div className="modal-content">

            <form onSubmit={this.submitListName}>
              <div className="field">
                <div className="control">
                  <input className="input" 
                         type="text" 
                         placeholder="Enter List name" 
                         name="list-name"
                         required
                  />
                </div>
              </div>
              <div className="field">
                <div className="control">
                  <button className="button is-link">
                    Add List
                  </button>
                </div>  
              </div>
            </form>

            {
              errorAddingList && (
                <button className="button is-danger is-light is-outlined is-static is-fullwidth is-medium mt-3">{errorAddingList}</button>
              )
            }

          </div>
          <button className="modal-close is-large" 
                  aria-label="close"
                  onClick={this.closeModal}
          ></button>
        </div>

      </div>
    );
  }
}

export default AddAList;
