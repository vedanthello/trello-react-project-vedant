import React, { Component } from "react";
import axios from "axios";
import app from "../keyAndToken.js";

class UpdateCardName extends Component {

  constructor(props) {
    super(props)
  
    this.state = {
      isEditing: false
    }
  }

  submitCardName = event => {
    event.preventDefault();
    const formData = new FormData(event.target);
    const submittedCardName = formData.get("card-name");
    
    // update the Card's name
    axios.put(`https://api.trello.com/1/cards/${this.props.cardId}?name=${submittedCardName}&key=${app.key}&token=${app.token}`)
    .then(response => {
      this.props.updateCardName(submittedCardName);
      this.closeEditor();
    })
    .catch(err => {
      console.log(err);
    });
  }

  openEditor = () => {
    this.setState({ isEditing: true });
  };

  closeEditor = () => {
    this.setState({ isEditing: false });
  };

  render() {
    return (
      <div className="UpdateCardName">
        {
          (this.state.isEditing === false) ? (
            <h1 className="modal-card-title" onClick={this.openEditor}>{this.props.cardName}</h1>
          ) : (
            <form onSubmit={this.submitCardName}>
              <div className="field">
                <div className="control">
                  <textarea className="textarea"
                            defaultValue={this.props.cardName}
                            name="card-name"
                            rows="2"
                            required
                  ></textarea>
                </div>
              </div>
              <div className="field is-grouped">
                <div className="control">
                  <button className="button is-link">
                    Update
                  </button>
                </div>
                <div className="control">
                  <div className="button" onClick={this.closeEditor}>
                    Cancel
                  </div>
                </div>
              </div>
            </form>
          )  
        }
      </div>
    );
  }
}

export default UpdateCardName;
